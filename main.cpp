#include "cvlibrary/includes/cvlib.hpp"
#include <stdio.h>
#include <cmath>
#include <opencv2/opencv.hpp>
#include <iostream>

#define UNUSED __attribute__((unused))

int		main(UNUSED int argc, UNUSED char **argv) {
  clframework *f = new clframework();
  f->newImage("images/train.png");

  // Applying box blur
  f->applyConvolution(CL_BIGGAUSSIAN);
  f->setResultAsNewOrigin();
  f->displayInput();

  f->applyConvolution(CL_SOBELSK);
  f->setResultAsNewOrigin();
  //f->displayInput();
  //  f->simpleTreshold();

  f->applyNMS();
  f->setResultAsNewOrigin();
  f->applyDTWH();
  //  f->setResultAsNewOrigin();
  //  f->displayInput();
  cv::waitKey();

  delete f;
  return 0;
}
