#include <stdio.h>
#include <cmath>
#include <opencv2/opencv.hpp>
#include <iostream>
#include "../includes/cvlib_image.hpp"

#define UNUSED __attribute__((unused))

void clImage::runDTWH(int y, int x) {
  std::cout << "DTWH y = " << y << " x : " << x << std::endl;
  float o = orientation.at<float>(y,x);
  cv::Mat ref = magnitude;
  uchar current = ref.at<uchar>(y,x);
  if ((o >= 0.00 && o < (M_PI/8))
      || (o >= (7*M_PI/8) && o < (9*M_PI/8))) {
    if (current < treshold) result.at<uchar>(y,x) = 0;
    else if (current > treshold && current < treshold2) {
    } else if (current > treshold2) result.at<uchar>(y,x) = 255;
    if (x - 1 > 0)
      runDTWH(y,x-1);
    if (x + 1 < ref.cols)
      runDTWH(y,x+1);
  } else if (o >= (M_PI/8) && o < (3*M_PI/8)) {
    if (current < treshold) result.at<uchar>(y,x) = 0;
    else if (current > treshold && current < treshold2) {
    } else if (current > treshold2) result.at<uchar>(y,x) = 255;
    if (y - 1 >= 0 && x + 1 <= ref.cols)
      runDTWH(y-1,x+1);
    if (y + 1 <= ref.rows && x - 1 >= 0)
      runDTWH(y+1,x-1);
  } else if (o >= (3*M_PI/8) && o < (5*M_PI/8)) {
    if (current < treshold) result.at<uchar>(y,x) = 0;
    else if (current > treshold && current < treshold2) {
    } else if (current > treshold2) result.at<uchar>(y,x) = 255;
    if (y - 1 >= 0)
      runDTWH(y-1,x);
    if (y + 1 <= ref.rows)
      runDTWH(y+1,x);
  } else if (o >= (5*M_PI/8) && o < (7*M_PI/8)) {
    if (current < treshold) result.at<uchar>(y,x) = 0;
    else if (current > treshold && current < treshold2) {
    } else if (current > treshold2) result.at<uchar>(y,x) = 255;
    if (y - 1 >= 0 && x - 1 >= 0)
      runDTWH(y-1,x-1);
    if (y + 1 <= ref.rows && x + 1 <= ref.cols)
      runDTWH(y+1,x+1);
  }
}

/*
  
 */
void clImage::doubleThresholdWithHysterysis() {
  treshold = 50;
  treshold2 = 150;
  std::cout << "Running DTWH" << std::endl;
  runDTWH(magnitude.rows/2,magnitude.cols/2);
  cv::imshow("DTWH", result);
}

bool clImage::applyNMS(int y, int x) {
  float o = orientation.at<float>(y,x);
  cv::Mat ref = magnitude;
  //cv::Mat ref = laplaceGradient;
  uchar m = ref.at<uchar>(y,x);
  // Cancel sign
  if (o < 0.00) {
    o = M_PI - sqrt(pow(o,2));
  }
  //  std::cout << "Atan " << o << std::endl;
  // float tresholdlocal = 120;
  
  bool reduce = false;
  //  std::cout << o << std::endl;
  if ((o >= 0.00 && o < (M_PI/8)) || (o >= (7*M_PI/8) && o < (9*M_PI/8))) {
    // Check Y axis
    float cmpTop = ref.at<uchar>(y,x-1);
    float cmpBottom = ref.at<uchar>(y,x+1);
    //   if (m >= cmpTop && m >= cmpBottom) result.at<uchar>(y,x) = sqrt(m*m);
    //    else result.at<uchar>(y,x) = 0;
    if (m < cmpTop || m < cmpBottom) result.at<uchar>(y,x) = 0;
    else {
      result.at<uchar>(y,x) = ref.at<uchar>(y,x);
      
      // if (m >= tresholdSimple) {
      // 	result.at<uchar>(y,x) = ref.at<uchar>(y,x);
      // } else {
      // 	result.at<uchar>(y,x) = 0;
      // }
    }
  } else if (o >= (M_PI/8) && o < (3*M_PI/8)) {
    // Check (Y-1,X-1) AND (Y+1,X+1)
    float cmpTop = ref.at<uchar>(y+1,x-1);
    float cmpBottom = ref.at<uchar>(y-11,x+1);
    //    if (m >= cmpTop && m >= cmpBottom) result.at<uchar>(y,x) = sqrt(m*m);
    //    else result.at<uchar>(y,x) = 0;
    if (m < cmpTop || m < cmpBottom) result.at<uchar>(y,x) = 0;
    else {
      result.at<uchar>(y,x) = ref.at<uchar>(y,x);
      
      // if (m >= tresholdSimple) {
      // 	result.at<uchar>(y,x) = ref.at<uchar>(y,x);
      // } else {
      // 	result.at<uchar>(y,x) = 0;
      // }
    }
  } else if (o >= (3*M_PI/8) && o < (5*M_PI/8)) {
    // Check X axis
    float cmpTop = ref.at<uchar>(y-1,x);
    float cmpBottom = ref.at<uchar>(y+1,x);
    //   if (m >= cmpTop && m >= cmpBottom) result.at<uchar>(y,x) = sqrt(m*m);
    //    else result.at<uchar>(y,x) = 0;
    if (m < cmpTop || m < cmpBottom) result.at<uchar>(y,x) = 0;
    else {
      result.at<uchar>(y,x) = ref.at<uchar>(y,x);
      
      // if (m >= tresholdSimple) {
      // 	result.at<uchar>(y,x) = ref.at<uchar>(y,x);
      // } else {
      // 	result.at<uchar>(y,x) = 0;
      // }
    }
  } else if (o >= (5*M_PI/8) && o < (7*M_PI/8)) {
    // Check (Y+1,X-1) AND (Y-1,X+1)
    float cmpTop = ref.at<uchar>(y-1,x-1);
    float cmpBottom = ref.at<uchar>(y+1,x+1);
    //    if (m >= cmpTop && m >= cmpBottom) result.at<uchar>(y,x) = sqrt(m*m);
    //    else result.at<uchar>(y,x) = 0;
    if (m < cmpTop || m < cmpBottom) result.at<uchar>(y,x) = 0;
    else {
      result.at<uchar>(y,x) = ref.at<uchar>(y,x);
      
      // if (m >= tresholdSimple) {
      // 	result.at<uchar>(y,x) = ref.at<uchar>(y,x);
      // } else {
      // 	result.at<uchar>(y,x) = 0;
      // }
    }
  }
  return true;
}

float clImage::getDirection(int y, int x) {
  // Compute tan with derivative to get a direction
  return atan2(x,y);
}

void clImage::nonMaxSuppression() {
  //  tresholdSimple = 50;
  result = cv::Mat(input.rows,input.cols, CV_8UC1, double(0));
  for (int y = 1; y < input.rows - 1; y++) {
    for (int x = 1; x < input.cols - 1; x++) {
      applyNMS(y,x);
    }
  }
  cv::imshow("nms", result);
  //  cv::waitKey(0);
}

void clImage::displayInCurrentWindow(clImage* self) {
  cv::imshow("simpleTreshold", self->result);
}

void clImage::applyTreshold(clImage* self) {
  //  std::cout << "Treshold " << self->treshold << std::endl;
  for (int y = 0; y < self->input.rows; y++) {
    for (int x = 0; x < self->input.cols; x++) {
      //      self->result.at<uchar>(y,x) = self->input.at<uchar>(y,x) >
      //	self->treshold ? self->input.at<uchar>(y,x) : 0;
      //      self->result.at<uchar>(y,x) = self->input.at<uchar>(y,x) >= self->treshold ? 255 : 0;
      //     self->tresholdSimple =
      //self->nonMaxSuppression();
    }
  }
  //  self->displayInCurrentWindow(self);
}
void clImage::updateDoubleTresholdInput(clImage* self) {
  self->result.copyTo(self->doubleTresholdInput);
}

void clImage::apply2ndTreshold(clImage* self) {
  for (int y = 0; y < self->input.rows; y++) {
    for (int x = 0; x < self->input.cols; x++) {
      self->result.at<uchar>(y,x) = self->input.at<uchar>(y,x) >
	//	self->treshold2 ? 255 : (self->input.at<uchar>(y,x) < self->treshold ? 0 : self->input.at<uchar>(y,x));
	self->treshold2 ? 255 : (self->input.at<uchar>(y,x) < self->treshold ? 0 : 255);
    }
  }
  self->displayInCurrentWindow(self);
}

void clImage::changeTrackbar(int position, void* inst) {
  clImage* self = (clImage*)inst;
  self->applyTreshold(self);
}

void clImage::changeTrackbar2(int position, void* inst) {
  clImage* self = (clImage*)inst;
  self->apply2ndTreshold(self);
}

void clImage::simpleTreshold() {
  //int createTrackbar(const string& trackbarname, const string& winname, int* value, int count, TrackbarCallback onChange=0, void* userdata=0)
  tresholdSimple = 1;
  cv::namedWindow("simpleTreshold");
  cv::createTrackbar("Treshold","simpleTreshold", &tresholdSimple, 255, &clImage::changeTrackbar, this);
  changeTrackbar(0, this);
  cv::waitKey(0);
}

void clImage::doubleTreshold() {
  treshold = 0;
  treshold2 = 255;
  input.copyTo(doubleTresholdInput);
  cv::namedWindow("doubleTreshold");
  cv::createTrackbar("Treshold 1", "doubleTreshold", &treshold, 255, &clImage::changeTrackbar2, this);
  cv::createTrackbar("Treshold 2", "doubleTreshold", &treshold2, 255, &clImage::changeTrackbar2, this);
  changeTrackbar2(0,this);
  cv::waitKey(0);
}

void clImage::displayMagnitude() {
  cv::imshow("Magnitude", magnitude);
  //  cv::waitKey(0);
} 

void clImage::displayOriginal() {
  cv::imshow("Original image", original);
  //  cv::waitKey(0);
}

void clImage::displayInput() {
  cv::imshow("Input image", input);
  //  cv::waitKey(0);
}

void clImage::displayResult() {
  cv::imshow(resultTitle, result);
  //  cv::waitKey(0);
}

void clImage::displayAll() {
  std::cout << " DEBUG 1 " << std::endl;
  if (original.empty() == false) displayOriginal();
  std::cout << " DEBUG 2 " << std::endl;
  if (input.empty() == false) displayInput();
  std::cout << " DEBUG 3 " << std::endl;
  if (result.empty() == false) displayResult();
  std::cout << "End display" << std::endl;
}

float clImage::computeNormalizedValue(kernel *k, int y, int x) {
  cv::Mat C = (cv::Mat_<double>(3,3) <<
	   0,-1,0,
	   -1,5,-1,
	   0,-1,0);
  float total = 0;
  int i = y - (3/2);
  int j = x - (3/2);
  int iMax = y + (3/2);
  int jMax = x + (3/2);
  int idx = 0;
  int vy = 0;
  int vx = 0;

  while (i <= iMax) {
    j -= 3;
    vx = 0;
    while (j <= jMax) {
      float tmp = ((float(input.at<uchar>(i,j)) *
		       float(C.at<double>(vy,vx))));
      total += tmp;
      idx++;
      j++;
      vx++;
    }
    i++;
    vy++;
  }
  //  std::cout << total/9 << std::endl;
  return total / 9;
}

float clImage::computeNormalizedValue(cv::Mat inp, cv::Mat kernel) {
  float total = 0;
  for (int y = 0; y < kernel.rows; y++) {
    for (int x = 0; x < kernel.cols; x++) {
      total += kernel.at<int>(y,x) * inp.at<uchar>(y,x);
    }
  }
  return total;
}

void clImage::running(kernel *k) {
  cv::Mat cp = (cv::Mat_<uchar>(3,3));
  for (int y = 0;
       y <= input.rows;
       y++) {
    for (int x = 0;
	 x <= input.cols;
	 x++) {
      result.at<uchar>(y,x) = input.at<uchar>(y,x) * 2 / 9;
      if (y == 0 || y >= input.rows-2) {
	result.at<uchar>(y,x) = 255;
      } else if (x == 0 || x >= input.cols-2) {
	result.at<uchar>(y,x) = 255;
      } else if (y > 0 && y < input.rows-3 && x > 0 && x < input.cols-3) {
        input(cv::Rect(x-1,y-1,3,3)).copyTo(cp);//X,Y ><
	//	result.at<uchar>(y,x) = computeNormalizedValue(cp);
      }
    }
  }
}

void clImage::running(cv::Mat kernel, int divider) {
  cv::Mat cp = (cv::Mat_<uchar>(kernel.rows,kernel.cols));
  for (int y = 0;
       y <= input.rows;
       y++) {
    for (int x = 0;
	 x <= input.cols;
	 x++) {
      if (y <= 0+(kernel.rows/2) || y >= (input.rows-(kernel.rows/2))) {
	result.at<uchar>(y,x) = 255;
      } else if (x <= 0+(kernel.cols/2) || x >= (input.cols-(kernel.cols/2))) {
	result.at<uchar>(y,x) = 255;
      } else if (y > 0 + (kernel.rows/2) && y < input.rows-(kernel.rows/2) && x > 0 + (kernel.cols/2) && x < input.cols-(kernel.cols/2)) {
        input(cv::Rect(x-(kernel.cols/2),y-(kernel.rows/2),kernel.cols,kernel.rows)).copyTo(cp);//X,Y ><
	int tmpv = computeNormalizedValue(cp,kernel) / divider;
	result.at<uchar>(y,x) = tmpv;
      }
    }
  }
}

void clImage::applyCustomKernel(int *kMat, int length, int divider) {
  kernel *k = new kernel(kMat,length,divider);

  result = cv::Mat(input.rows, input.cols, input.type());
  running(NULL);
  //delete k;
  divide = false;
  displayResult();
}

void clImage::applyCustomKernel(cv::Mat kernel, int divider) {
  initResult();
  running(kernel, divider);
}

void clImage::setDivide(bool is) {
  divide = is;
}

bool clImage::mustDivide(void) {
  return divide;
}

void clImage::applyGaussianBlurKernel() {
  cv::Mat C = (cv::Mat_<int>(5,5,CV_32F) <<
	       1,4,6,4,1,
	       4,16,24,16,4,
	       6,24,36,24,6,
	       4,16,24,16,4,
	       1,4,6,4,1);

  resultTitle = "Gaussian Blur Matrix Result";
  applyCustomKernel(C,256);
}

void clImage::applyBoxBlurKernel() {
  cv::Mat C = (cv::Mat_<int>(3,3,CV_32F) <<
	       1,1,1,
	       1,1,1,
	       1,1,1);

  resultTitle = "Box Blur Matrix Result";
  applyCustomKernel(C,9);
}

void clImage::applyIdentityKernel() {
  cv::Mat C = (cv::Mat_<int>(3,3,CV_32F) <<
	       0,0,0,
	       0,1,0,
	       0,0,0);

  resultTitle = "Identity Matrix Result";
  applyCustomKernel(C,1);
}

void clImage::applySharpenKernel() {
  cv::Mat C = (cv::Mat_<int>(3,3,CV_32F) <<
  	       0,-1,0,
  	       -1,5,-1,
  	       0,-1,0);

  resultTitle = "Sharpen Matrix Result";
  applyCustomKernel(C,9);
}
float clImage::computeNormalizedValue(cv::Mat inp, cv::Mat Gx,
				      cv::Mat Gy) {
  float total = 0;
  for (int y = 0; y < Gx.rows; y++) {
    for (int x = 0; x < Gx.cols; x++) {
      total += Gx.at<int>(y,x) * inp.at<uchar>(y,x) + Gy.at<int>(y,x) * inp.at<uchar>(y,x);
    }
  }
  total = total;
  //  if (total < 0) total = 0;
  return total;
}

void clImage::runMultiKernels(cv::Mat Gx, cv::Mat Gy) {
  cv::Mat cp = (cv::Mat_<uchar>(Gx.rows,Gx.cols));
  sobelx = cv::Mat(input.rows,input.cols,CV_8SC1);
  sobely = cv::Mat(input.rows,input.cols,CV_8SC1);
  for (int y = 0;
       y <= input.rows;
       y++) {
    for (int x = 0;
	 x <= input.cols;
	 x++) {
      if (y <= 0+(Gx.rows/2) || y >= (input.rows-(Gx.rows/2))) {
	result.at<int>(y,x) = 128;
      } else if (x <= 0+(Gx.cols/2) || x >= (input.cols-(Gx.cols/2))) {
	result.at<int>(y,x) = 128;
      } else if (y > 0 + (Gx.rows/2) && y < input.rows-(Gx.rows/2) &&
		 x > 0 + (Gx.cols/2) && x < input.cols-(Gx.cols/2)) {
        input(cv::Rect(x-(Gx.cols/2),y-(Gx.rows/2),Gx.cols,Gx.rows))
	  .copyTo(cp);//X,Y ><
	//std::cout << "CP " << cp << std::endl;
	float ox = computeNormalizedValue(cp,Gx);
	float oy = computeNormalizedValue(cp,Gy);
	// if (ox < 0) ox = sqrt(ox*ox);
	// if (oy < 0) oy = sqrt(oy*oy);
	//	if (ox < 0) ox = ox + 127;
	//	if (oy < 0) oy = oy + 127;
	//	ox += 127;
	//	oy += 127;
        sobelx.at<char>(y,x) = ox;
	sobely.at<char>(y,x) = oy;
        //std::cout << "SX " << int(sobelx.at<char>(y,x)) << " SY " << int(sobely.at<char>(y,x)) << std::endl;
      }
    }
  }
}

void clImage::applySobelKernels(cv::Mat Gx, cv::Mat Gy) {
  result = cv::Mat(input.rows, input.cols, CV_8S);// FIXME : Make an auto creator
  runMultiKernels(Gx,Gy);
}

// TOGO
void clImage::computeMagnitude() {
  magnitude = cv::Mat(input.rows,input.cols,CV_8UC1);
  orientation = cv::Mat(input.rows,input.cols,CV_32F);
  orientationArray = cv::Mat(input.rows, input.cols, CV_32F);
  for (int y = 0; y < input.rows; y++) {
    for (int x = 0; x < input.cols; x++) {
      //std::cout << "Magnitude of sqrt(" << float(pow(sobelx.at<char>(y,x),2)) << " + " << float(pow(sobely.at<char>(y,x),2)) << ") = " << float(sqrt(pow(sobelx.at<char>(y,x),2)+ pow(sobely.at<char>(y,x),2))) << std::endl;
      magnitude.at<uchar>(y,x) = sqrt(pow(sobelx.at<char>(y,x),2)+
				   pow(sobely.at<char>(y,x),2));
      //std::cout << "Calc orient ("<<int(sobely.at<char>(y,x))<<") ("<<int(sobelx.at<char>(y,x))<<")"<<std::endl;
      
      orientation.at<float>(y,x) = atan2(sobely.at<char>(y,x)>0?sobely.at<char>(y,x):-sobely.at<char>(y,x), sobelx.at<char>(y,x));
      //std::cout << "Orientation ("<<y<<","<<x<<") = " << orientation.at<float>(y,x) << std::endl;
    }
  }
}

void clImage::applyEdgeSobelOperatorKernels() {
  cv::Mat Gx = (cv::Mat_<int>(3,3,CV_32F) <<
		1,0,-1,
		2,0,-2,
		1,0,-1);
  cv::Mat Gy = (cv::Mat_<int>(3,3,CV_32F) <<
		1,2,1,
		0,0,0,
		-1,-2,-1);
  applySobelKernels(Gx,Gy);
  //  colourDirections();
  cv::imshow("sobelx image", sobelx);
  cv::imshow("sobely image", sobely);
  //  cv::imshow("Orientation", orientation);
  //  cv::waitKey(0);
  computeMagnitude();
  displayMagnitude();
  magnitude.copyTo(result);
}

void clImage::applyLaplaceOperatorKernel() {
  cv::Mat laplace = (cv::Mat_<int>(3,3,CV_32F) <<
		     0,1,0,
		     1,-4,1,
		     0,1,0);
  applyCustomKernel(laplace,1);
  result.copyTo(laplaceGradient);
  //cv::imshow("Laplaced", result);
  //cv::waitKey(0);
}

void clImage::colourDirections() {
  std::cout << "Original Type : " << GetMatType(original) << " and Depth : " << GetMatDepth(original) << std::endl;
  std::cout << "Magnitude Type : " << GetMatType(magnitude) << " and Depth : " << GetMatDepth(magnitude) << std::endl;
  for (int y = 0; y < original.rows; y++) {
    for (int x = 0; x < original.cols; x++) {
      // Useless computation lol
    }
  }
}

clImage::clImage() {
  std::cout << "clImage begin" << std::endl;
}

clImage::clImage(std::string const &path) {
  original = cv::imread( path, CV_LOAD_IMAGE_COLOR );
  if (original.empty() == true) {
    std::cout << "Impossible to load image " << path << std::endl;
    return ;
  }
  cv::cvtColor(original, input_ori, CV_BGR2GRAY);
  input = cv::Mat();
  input_ori.copyTo(input);
  result = cv::Mat();
  initResult();
}

void clImage::initResult() {
  result = cv::Mat(input_ori.rows, input_ori.cols, CV_8UC1);
}

void clImage::updateInput() {
  result.copyTo(input);
}

clImage::~clImage() {
  std::cout << "clImage finishing" << std::endl;
}

int kernel::Rows() {
  return rows;
}

int kernel::Cols() {
  return cols;
}

int kernel::Length() {
  return length;
}

int kernel::Divider() {
  return divider;
}

kernel::kernel(int *_mat, int size, int divider) {
  matrix = new int[sizeof(int)*size];;
  for (int i = 0; i < size; i++) {
    matrix[i] = _mat[i];
  }
  rows = sqrt(size);
  cols = sqrt(size);
  length = size;
  divider = divider;
}

kernel::~kernel() {
  //  delete matrix;
}

std::string GetMatDepth(const cv::Mat& mat)
{
    const int depth = mat.depth();

    switch (depth)
    {
    case CV_8U:  return "CV_8U";
    case CV_8S:  return "CV_8S";
    case CV_16U: return "CV_16U";
    case CV_16S: return "CV_16S";
    case CV_32S: return "CV_32S";
    case CV_32F: return "CV_32F";
    case CV_64F: return "CV_64F";
    default:
        return "Invalid depth type of matrix!";
    }
}

std::string GetMatType(const cv::Mat& mat)
{
    const int mtype = mat.type();

    switch (mtype)
    {
    case CV_8UC1:  return "CV_8UC1";
    case CV_8UC2:  return "CV_8UC2";
    case CV_8UC3:  return "CV_8UC3";
    case CV_8UC4:  return "CV_8UC4";

    case CV_8SC1:  return "CV_8SC1"; // (Gx and Gy for sign conservation?)
    case CV_8SC2:  return "CV_8SC2";
    case CV_8SC3:  return "CV_8SC3";
    case CV_8SC4:  return "CV_8SC4";

    case CV_16UC1: return "CV_16UC1";
    case CV_16UC2: return "CV_16UC2";
    case CV_16UC3: return "CV_16UC3";
    case CV_16UC4: return "CV_16UC4";

    case CV_16SC1: return "CV_16SC1";
    case CV_16SC2: return "CV_16SC2";
    case CV_16SC3: return "CV_16SC3";
    case CV_16SC4: return "CV_16SC4";

    case CV_32SC1: return "CV_32SC1";
    case CV_32SC2: return "CV_32SC2";
    case CV_32SC3: return "CV_32SC3";
    case CV_32SC4: return "CV_32SC4";

    case CV_32FC1: return "CV_32FC1";
    case CV_32FC2: return "CV_32FC2";
    case CV_32FC3: return "CV_32FC3";
    case CV_32FC4: return "CV_32FC4";

    case CV_64FC1: return "CV_64FC1";
    case CV_64FC2: return "CV_64FC2";
    case CV_64FC3: return "CV_64FC3";
    case CV_64FC4: return "CV_64FC4";

    default:
        return "Invalid type of matrix!";
    }
}
