#include <stdio.h>
#include <cmath>
#include <opencv2/opencv.hpp>
#include "../includes/cvlib.hpp"
#include <iostream>

void clframework::applyDTWH() {
  image->doubleThresholdWithHysterysis();
}

void clframework::applyNMS() {
  image->nonMaxSuppression();
}

void clframework::simpleTreshold() {
  image->simpleTreshold();
}

void clframework::doubleTreshold() {
  image->doubleTreshold();
}

bool clframework::applyConvolution(int idx) {
  switch (idx) {
  case 0:
    image->applyIdentityKernel();
    break;
  case 1:
    image->applySharpenKernel();
    break;
  case 2:
    image->applyBoxBlurKernel();
    break;
  case 3:
    image->applyGaussianBlurKernel();
    break;
  case 4:
    image->applyEdgeSobelOperatorKernels();
    break;
  case 5:
    image->applyLaplaceOperatorKernel();
    break;
  }
  return true;
}

// Call to a function that set next input image as the result of previous
// computation
// & clear the result image of previous computation
void clframework::setResultAsNewOrigin() {
  image->updateInput();
  image->initResult();
}

// Must let the user choose the kernel via a limited shell
void clframework::chooseKernel() {
  // Propose the kernels usable and apply it.
}

bool clframework::newImage(std::string path) {
  if (image != NULL) {
    // Error
    std::cout << "Error clframework::newImage can't allocate new clImage if already existing" << std::endl;
    return false;
  }
  image = new clImage(path);
  if (image == NULL) {
    // Error
    std::cout << "Error allocating new clImage object" << std::endl;
    return false;
  }
  std::cout << "New clImage created" << std::endl;
  return true;
}

bool clframework::deleteImage() {
  if (image != NULL) {
    delete image;
  }
  std::cout << "Deleted clImage" << std::endl;
  return true;
}

void clframework::displayInput() {
  image->displayInput();
}

void clframework::displayResult() {
  image->displayResult();
}

void clframework::displayImage() {
  image->displayAll();
  //  std::cout << "Should display the image" << std::endl;
}

clframework::clframework() {
  std::cout << "Framework begin" << std::endl;
  image = NULL;
}

clframework::~clframework() {
  std::cout << "Framework finished" << std::endl;
}


