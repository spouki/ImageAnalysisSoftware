

CC	= g++

RM	= rm -f

NAME	= exe

SRC	= ./main.cpp \
	  ./cvlibrary/sources/cvlib.cpp \
	  ./cvlibrary/sources/cvlib_image.cpp \

OBJ	= $(SRC:.cpp=.o)

#CFLAGS = -std=c++11 -I/usr/include/opencv

CFLAGS	=	-std=c++11 -pedantic -Wconversion -Wsign-conversion -O0 -g -W -Wall -Wextra -ggdb3
CFLAGS +=	-fexceptions -I/usr/include/opencv 

LFLAGS +=	-lopencv_calib3d -lopencv_contrib -lopencv_core -lopencv_features2d -lopencv_flann -lopencv_gpu -lopencv_highgui -lopencv_imgproc -lopencv_legacy -lopencv_ml -lopencv_nonfree -lopencv_objdetect -lopencv_ocl -lopencv_photo -lopencv_stitching -lopencv_superres -lopencv_ts -lopencv_video -lopencv_videostab -ltbb

ifeq ($(OS),Windows_NT)
    detected_OS := Windows
else
    detected_OS := $(shell sh -c 'uname -s 2>/dev/null || echo not')
endif

ifeq ($(detected_OS),Windows)
    #CFLAGS += -D WIN32
endif
ifeq ($(detected_OS),Darwin)  # Mac OS X
    CFLAGS   += -D APPLE
endif
ifeq ($(detected_OS),Linux)
#    CFLAGS   +=   -D Linux
endif
ifeq ($(detected_OS),GNU)           # Debian GNU Hurd
    #CFLAGS   +=   -D GNU_HURD
endif
ifeq ($(detected_OS),GNU/kFreeBSD)  # Debian kFreeBSD
    #CFLAGS   +=   -D GNU_kFreeBSD
endif
ifeq ($(detected_OS),FreeBSD)
    #CFLAGS   +=   -D FreeBSD
endif
ifeq ($(detected_OS),NetBSD)
    #CFLAGS   +=   -D NetBSD
endif
ifeq ($(detected_OS),DragonFly)
    #CFLAGS   +=   -D DragonFly
endif
ifeq ($(detected_OS),Haiku)
    #CFLAGS   +=   -D Haiku
endif


all:		$(NAME)

$(NAME):	$(OBJ)
	@$(CC) $(CFLAGS) -o $(NAME) $(OBJ) $(LFLAGS)

clean:
	$(RM) $(OBJ)

fclean:	clean
	$(RM) $(NAME)

re: fclean all
