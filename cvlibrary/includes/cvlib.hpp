#ifndef __CVLIB_HPP__
#define __CVLIB_HPP__

#include "cvlib_image.hpp"
#include <iostream>
#include <opencv2/opencv.hpp>

#define CL_IDENTITYK 0
#define CL_SHARPENK 1
#define CL_BOXBLURK 2
#define CL_BIGGAUSSIAN 3
#define CL_SOBELSK 4
#define CL_LAPLACEK 5

class clframework {
public:
  clframework();
  ~clframework();
  bool newImage(std::string);
  bool deleteImage();
  void displayImage();
  void displayInput();
  void displayResult();
  void chooseKernel();
  bool applyConvolution(int);
  void setResultAsNewOrigin();
  void simpleTreshold();
  void doubleTreshold();
  void applyNMS();
  void applyDTWH();

private:
  clImage *image;
};


#endif /* __CVLIB_HPP__ */

