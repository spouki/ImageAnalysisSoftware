#ifndef __CVLIB_IMAGE_HPP__
#define __CVLIB_IMAGE_HPP__
#include <iostream>
#include <opencv2/opencv.hpp>

#define CL_IDENTITYK 0
#define CL_SHARPENK 1
#define CL_BOXBLURK 2
#define CL_BIGGAUSSIAN 3
#define CL_SOBELSK 4
#define CL_LAPLACEK 5

// Mettre en place les const

class kernel {
public:
  kernel(int*,int,int);
  ~kernel();
  int Rows();
  int Cols();
  int Length();
  int Divider();
  int *matrix;
private:
  int rows;
  int cols;
  int length;
  int divider;
};

class clImage {
public:
  clImage();
  clImage(std::string const &);
  ~clImage();

  void displayOriginal();
  void displayGrey();
  void displayResult();
  void displayInput();
  void displayAll();
  static void displayInCurrentWindow(clImage*);

  void applyIdentityKernel();
  void applySharpenKernel();
  void applyBoxBlurKernel();
  void applyGaussianBlurKernel();
  void applyEdgeSobelOperatorKernels();
  void applySobelKernels(cv::Mat, cv::Mat);
  void applyLaplaceOperatorKernel();

  void applyCustomKernel(int*,int,int);
  void applyCustomKernel(cv::Mat,int);

  void running(kernel*);
  void running(cv::Mat, int);
  void runMultiKernels(cv::Mat,cv::Mat);

  float computeNormalizedValue(kernel*,int,int);
  float computeNormalizedValue(cv::Mat,cv::Mat);
  float computeNormalizedValue(cv::Mat,cv::Mat,cv::Mat);

  bool mustDivide();
  void setDivide(bool);

  void computeMagnitude();
  void displayMagnitude();

  void colourDirections(); // Not implemented, need to concept it 

  // Fill input with result's content
  // And clear result with zeros
  void updateInput();
  void initResult();

  void simpleTreshold();
  static void applyTreshold(clImage*);
  static void changeTrackbar(int,void*);
  void doubleTreshold();
  static void apply2ndTreshold(clImage*);
  static void changeTrackbar2(int,void*);
  static void updateDoubleTresholdInput(clImage*);

  void nonMaxSuppression();
  //
  float getDirection(int,int);
  bool applyNMS(int,int);
  

  void runDTWH(int,int);
  void doubleThresholdWithHysterysis();
  
private:
  // Original image
  cv::Mat original;

  // B&W images used for computation
  // input_ori is the B&W original # Don't touch if want to go to first state
  cv::Mat input_ori;
  // input is the used B&W image for computation of result
  cv::Mat input;
  cv::Mat doubleTresholdInput;

  // B&W resulting image of computation
  cv::Mat result;


  // Sobels' related matrix
  cv::Mat sobelx;
  cv::Mat sobely;
  cv::Mat magnitude;
  cv::Mat laplaceGradient;
  

  // Might be used for calculating orientation of edge ...
  cv::Mat orientationArray;
  cv::Mat orientation;

  // Result of Non Maximum Suppression
  cv::Mat nms;

  std::string resultTitle;
  bool divide;

  int treshold;
  int treshold2;
  int tresholdSimple;
};

std::string GetMatDepth(const cv::Mat&);
std::string GetMatType(const cv::Mat&);

#endif /* __CVLIB_IMAGE_HPP__ */
